<?php
namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Country;
use App\Entity\Phrase;
use App\Entity\Translation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPhrases extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $phrase = new Phrase();
        $phrase->setName('Kommst du aus Duisburg?');

        $categoryTwo = new Category();
        $categoryTwo->setCategory('Ausgabe');

        $categoryOne = new Category();
        $categoryOne->setCategory('Anmeldung');
        $manager->persist($categoryOne);

        $phrase->setCategory($categoryTwo);

        $translation = New Translation();
        $translation->setLanguageKey('gb');
        $translation->setTranslation('You come from Duisburg?');
        $translation->setPhrase($phrase);
        $manager->persist($translation);

        $manager->persist($phrase);

        $country = new Country();
        $country->setName('Englisch');
        $country->setLanguageKey('gb');
        $manager->persist($country);

        $country = new Country();
        $country->setName('Französisch');
        $country->setLanguageKey('fr');
        $manager->persist($country);

        $country = new Country();
        $country->setName('Niederländisch');
        $country->setLanguageKey('nl');
        $manager->persist($country);

        $manager->flush();
    }
}