<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ApiResource()
 */
class Country
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write"})
     * @MaxDepth(1)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write"})
     * @MaxDepth(1)
     */
    private $languageKey;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLanguageKey(): string
    {
        return $this->languageKey;
    }

    /**
     * @param string $languageKey
     */
    public function setLanguageKey(string $languageKey): void
    {
        $this->languageKey = $languageKey;
    }
}