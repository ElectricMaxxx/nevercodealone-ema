<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ApiResource(attributes={
 *     "normalization_context"={"read", "groups"={"category", "translation"}},
 *     "denormalization_context"={"write", "groups"={"category", "translation"}}
 * })
 */
class Phrase
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "category"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write", "category"})
     */
    private $name;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="phrases", cascade="persist")
     * @Groups({"read", "write", "category"})
     * @MaxDepth(1)
     * @ApiSubresource(maxDepth=1)
     */
    private $category;

    /**
     * @var Translation[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Translation", mappedBy="phrase", cascade="persist")
     * @Groups({"read", "write", "translation"})
     * @MaxDepth(1)
     */
    private $translations;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Translation[]|ArrayCollection
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param Translation[]|ArrayCollection $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }

}