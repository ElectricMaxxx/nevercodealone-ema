<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ApiResource(
 *     attributes={
 *         "normalization_context"={"read", "category"},
 *         "denormalization_context"={"write","category"}
 *     }
 *   )
 */
class Category
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "category"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write", "category"})
     * @MaxDepth(1)
     */
    private $category;

    /**
     * @var Phrase
     * @ORM\OneToMany(targetEntity="Phrase", mappedBy="category", cascade="persist")
     * @Groups({"read", "write"})
     */
    private $phrases;

    public function __construct()
    {
        $this->phrases = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory(string $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Phrase
     */
    public function getPhrases()
    {
        return $this->phrases;
    }

    /**
     * @param Phrase $phrases
     */
    public function setPhrases(Phrase $phrases): void
    {
        $this->phrases = $phrases;
    }
}