<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ApiResource
 */
class Translation
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"read", "translation"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write", "translation"})
     */
    private $translation;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"read", "write", "translation"})
     */
    private $languageKey;

    /**
     * @var Phrase
     * @ORM\ManyToOne(targetEntity="Phrase", inversedBy="translations", cascade="persist")
     * @Groups({"read", "write"})
     * @MaxDepth(1)
     */
    private $phrase;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTranslation(): string
    {
        return $this->translation;
    }

    /**
     * @param string $translation
     */
    public function setTranslation(string $translation): void
    {
        $this->translation = $translation;
    }

    /**
     * @return string
     */
    public function getLanguageKey(): string
    {
        return $this->languageKey;
    }

    /**
     * @param string $languageKey
     */
    public function setLanguageKey(string $languageKey): void
    {
        $this->languageKey = $languageKey;
    }

    /**
     * @return Phrase
     */
    public function getPhrase(): Phrase
    {
        return $this->phrase;
    }

    /**
     * @param Phrase $phrase
     */
    public function setPhrase(Phrase $phrase): void
    {
        $this->phrase = $phrase;
    }



}