# Emma API - Backend for Emma-Angular Application

This little API, driven by Symfony 4.0 and ApiPlattformBundle, is the backend for our [emma-angular](../frontend) application and serves
phrases, categories and countries to feed the frontend.
