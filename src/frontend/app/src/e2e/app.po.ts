import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getCountrySlectionItems() {
    return element.all(by.css('app-root app-country-selection app-country-selection-item'));
  }

  getCountrySelectionButtonElement() {
    return element(by.css('app-root app-country-selection button.btn'));
  }

  getCountrySelectionElement() {
    return element(by.css('app-root app-country-selection'));
  }
}

export class PhraseCollection {
  constructor (public page: AppPage) {}

  getPhraseCollection() {
    return element(by.css('app-root app-phrase-collection'));
  }

  getPhraseCollectionItems() {
    return element.all(by.css('app-root app-phrase-collection app-phrase-item'));
  }

  getPhraseCategoriesInPhraseCollection() {
    return element.all(by.css('div.phrase-category'));
  }
}

export class PhraseCreationForm {
  constructor (public phraseCollection) {}
  getFormElement() {
    return this.phraseCollection.getPhraseCollection().element(by.tagName('form'));
  }

  getSelectElement() {
    return this.getFormElement().element(by.tagName('select'));
  }

  getInputElement() {
    return this.getFormElement().element(by.tagName('input'));
  }
}
