import { AppPage, PhraseCreationForm, PhraseCollection } from './app.po';
import { CountrySelectionItemComponent } from '../src/app/components';

describe('Emma Frontend', () => {
  let page: AppPage;
  let form: PhraseCreationForm;
  let phraseCollection: PhraseCollection;
  beforeEach(() => {
    page = new AppPage();
    phraseCollection = new PhraseCollection(page);
    form = new PhraseCreationForm(phraseCollection);
  });

  describe('Start Page with country selection', () => {
    beforeEach(() => {
      page.navigateTo();
    });

    it('should display country selection section', () => {
      expect(page.getCountrySelectionButtonElement().isDisplayed()).toBeTruthy();
    });

    it('should display "Hide Country Selection" on button when visible', () => {
      expect(page.getCountrySelectionButtonElement().getText()).toContain('Hide');
    });

    it('should display 2 country for a country/language selection', () => {
      expect(page.getCountrySlectionItems().count()).toBe(2);
    });
  });

  describe('Select englisch country from country selection', () => {
    beforeEach(() => {
      page.navigateTo();
      page.getCountrySlectionItems().first().click();
    });

    it('should display phrase selction', () => {
      expect(phraseCollection.getPhraseCollection().isDisplayed()).toBeTruthy();
    });

    it('should display 4 cards with phrases', () => {
      expect(phraseCollection.getPhraseCollectionItems().count()).toBe(4);
    });

    it('should be separated into two categories', () => {
      expect(phraseCollection.getPhraseCategoriesInPhraseCollection().count()).toBe(2);
    });
    describe('Form to add a phrase', () => {
      it ('should display a form beside the phrase collection', () => {
        expect(form.getFormElement().isDisplayed()).toBeTruthy();
      });
      it('should offer a category selection with 2 categories to select', () => {
        expect(form.getSelectElement().isDisplayed()).toBeTruthy();
      });

      it('should offer a input to enter a new phrase', () => {
        expect(form.getInputElement().isDisplayed()).toBeTruthy();
      });
    });
  });
});
