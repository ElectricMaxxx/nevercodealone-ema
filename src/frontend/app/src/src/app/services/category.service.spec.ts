import { CategoryService } from './category.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('CategoryService', () => {
    let service: CategoryService;
    let httpClientMock;
    beforeEach(() => {
        httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
        service = new CategoryService(httpClientMock);
    });

    it('should call get method on http client', (done: DoneFn) => {
        const stubValue = [{category: 'test-message'}];
        httpClientMock.get.and.returnValue((of(stubValue)));
        service.getList().subscribe(value => {
            expect(value).toBe(stubValue);
            done();
        });
    });
});