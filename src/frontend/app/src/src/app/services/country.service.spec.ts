import { CountryService } from './country.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('CategoryService', () => {
    let service: CountryService;
    let httpClientMock;
    beforeEach(() => {
        httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
        service = new CountryService(httpClientMock);
    });

    it('should call get method on http client', (done: DoneFn) => {
        const stubValue = [{name: 'test-country', languageKey: 'de_DE'}];
        httpClientMock.get.and.returnValue((of(stubValue)));
        service.getList().subscribe(value => {
            expect(value).toBe(stubValue);
            done();
        });
    });
});
