import {CountryService} from './country.service';
import {PhraseService} from './phrase.service';
import {CategoryService} from './category.service';
import { TranslationService } from './translation.service.';

export const services: any[] = [
    CountryService,
    PhraseService,
    CategoryService,
    TranslationService
];

export * from './country.service';
export * from './phrase.service';
export * from './category.service';
export * from './translation.service.';
