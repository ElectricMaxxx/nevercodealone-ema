import {Injectable } from '@angular/core';
import {Translation} from '../models/translation.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';


import { environment } from '../../environments/environment';

@Injectable()
export class TranslationService {

    constructor (private http: HttpClient) {}

    getList(): Observable<Translation[]> {
        return this.http
          .get<Translation[]>(environment.apiUrl + `/translations`)
          .pipe(catchError((error: any) =>  Observable.throw(error)));
    }
}
