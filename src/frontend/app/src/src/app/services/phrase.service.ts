import {Injectable} from '@angular/core';
import {Phrase} from '../models/phrase.model';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';

@Injectable()
export class PhraseService {

    constructor (private http: HttpClient) {}

    private generateIRI(resource: string, id: number): string {
        return environment.apiUrl + '/' + resource + '/' + id.toString();
    }

    getList(): Observable<Phrase[]> {
        return this.http
          .get<Phrase[]>(environment.apiUrl + `/phrases`)
          .pipe(catchError((error: any) =>  Observable.throw(error)));
    }

    getPhrase(id: number): Observable<Phrase> {
        return this.http.get <Phrase>(environment.apiUrl + '/phrases/' + id);
    }

    addPhrase(phrase: Phrase): Observable<{} | Phrase> {
        return this.http
        .post<Phrase>(environment.apiUrl + `/phrases`, {name: phrase.name, category: phrase.category, translations: phrase.translations})
        .pipe(catchError((error: any) => {
            return Observable.throw(error);
        }));
    }

    removePhrase(phrase: Phrase): Observable<Phrase> {
        return this.http.delete <Phrase>(environment.apiUrl + '/phrases/' + phrase.id);
    }
}
