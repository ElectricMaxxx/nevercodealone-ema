import {Injectable} from '@angular/core';
import {Country} from '../models/country.model';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';
@Injectable()
export class CountryService {
    constructor (private http: HttpClient) {}

    getList(): Observable<Country[]> {
        return this.http
          .get<Country[]>(environment.apiUrl + `/countries`)
          .pipe(catchError((error: any) => Observable.throw(error)));
    }
}
