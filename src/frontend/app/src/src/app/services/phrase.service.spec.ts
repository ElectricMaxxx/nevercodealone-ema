import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs/observable/of';
import { PhraseService } from './phrase.service';

describe('PhraseService', () => {
    let service: PhraseService;
    let httpClientMock;
    beforeEach(() => {
        httpClientMock = jasmine.createSpyObj('HttpClient', ['get', 'post']);
        service = new PhraseService(httpClientMock);
    });

    it('should call get method on http client', (done: DoneFn) => {
        const stubValue = [{name: 'test-message', category: {category: 'text-category'}, translations: []}];
        httpClientMock.get.and.returnValue((of(stubValue)));
        service.getList().subscribe(value => {
            expect(value).toBe(stubValue);
            done();
        });
    });

    it('should call post method on http client', (done: DoneFn) => {
        const phrase = {name: 'test-message', category: {category: 'text-category'}, translations: [], id: 11};
        const stubValue = [{...phrase}];
        httpClientMock.post.and.returnValue((of(stubValue)));
        service.addPhrase(phrase).subscribe(value => {
            expect(value).toBe(stubValue);
            done();
        });
    });
});
