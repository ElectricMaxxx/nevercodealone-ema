import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { TranslationService } from '.';

describe('TranslationService', () => {
    let service: TranslationService;
    let httpClientMock;
    beforeEach(() => {
        httpClientMock = jasmine.createSpyObj('HttpClient', ['get']);
        service = new TranslationService(httpClientMock);
    });

    it('should call get method on http client', (done: DoneFn) => {
        const stubValue = [{translation: 'test-message', languageKey: 'de_DE'}];
        httpClientMock.get.and.returnValue((of(stubValue)));
        service.getList().subscribe(value => {
            expect(value).toBe(stubValue);
            done();
        });
    });
});
