import {Injectable} from '@angular/core';
import {Category} from '../models/category.model';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import 'rxjs/add/observable/throw';

import { environment } from '../../environments/environment';

@Injectable()
export class CategoryService {
    constructor (private http: HttpClient) {}

    getList(): Observable<Category[]> {
        return this.http
          .get<Category[]>(environment.apiUrl + `/categories`)
          .pipe(catchError((error: any) => {
              return Observable.throw(error);
          }
        ));
    }

    addCategory(category: Category): Observable<{} | Category> {
        return this.http
            .post<Category>(environment.apiUrl + `/category`, {category: category.category, description: category.description})
            .pipe(catchError((error: any) => {
                return Observable.throw(error);
            }));
    }
}
