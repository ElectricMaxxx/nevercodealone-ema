import {AppComponent} from './app.component';
import {CountrySelectionComponent} from './country.selection.component';
import {CountrySelectionItemComponent} from './country-selection-item.component';
import {PhraseCollectionComponent} from './phrase-collection.component';
import {PhraseItemComponent} from './phrase-item.component';

export const components: any[] = [
    AppComponent,
    CountrySelectionComponent,
    CountrySelectionItemComponent,
    PhraseCollectionComponent,
    PhraseItemComponent,
];

export * from './app.component';
export * from './country.selection.component';
export * from './country-selection-item.component';
export * from './phrase-collection.component';
export * from './phrase-item.component';
