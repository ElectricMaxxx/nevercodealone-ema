import {Component, EventEmitter, Input, Output, ChangeDetectionStrategy} from '@angular/core';
import {Country} from '../models/country.model';

@Component({
    selector: 'app-country-selection-item',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class="card">
            <div class="card-body" (click)="onSelectCountry()">
                <div class="card-img-top">
                    <img
                        width="150px" height="150px"
                        src="assets/svg-country-flags/{{country.languageKey}}.svg"
                        alt="{{country.name}}"
                        title="Wähle {{country.name}}" />
                </div>
                <div class="card-title">
                    <h4>{{country.name}}</h4>
                </div>
            </div>
        </div>
    `
})
export class CountrySelectionItemComponent {
    @Input() country: Country = null;
    @Output() selectCountry: EventEmitter<Country> = new EventEmitter<Country>();

    onSelectCountry(): void {
        this.selectCountry.emit(this.country);
    }
}
