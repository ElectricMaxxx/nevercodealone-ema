import {Component, Input, OnChanges, OnInit, SimpleChanges, ChangeDetectionStrategy} from '@angular/core';
import {Phrase} from '../models/phrase.model';
import {Country} from '../models/country.model';
import {Translation} from '../models/translation.model';

@Component({
    selector: 'app-phrase-item',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class="card mb-1" *ngIf="phrase">
            <h6 class="card-header">{{phrase.name}}</h6>
            <div class="card-body">
                <div class="card-text">
                    <span *ngIf="translation">{{translation.translation}}</span>
                    <span class="text-danger" *ngIf="!translation">No Translation</span>
                </div>
            </div>
        </div>
    `
})
export class PhraseItemComponent implements OnInit, OnChanges {
    @Input() phrase: Phrase;
    @Input() selectedCountry: Country;

    translation: Translation;

    ngOnInit(): void {
        this.evalutateTranslation();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.evalutateTranslation();
    }

    private evalutateTranslation(): void {
        if (!this.phrase || !this.selectedCountry) {
            return;
        }
        const translations = this.phrase.translations.filter(translation => translation.languageKey === this.selectedCountry.languageKey);
        if (!translations.length) {
            return;
        }

        this.translation = translations.shift();
    }
}