import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Component, NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store';
import { Observer } from 'rxjs';
import { CountryService, CategoryService, TranslationService } from '../services';

describe('AppComponent', () => {
  let countryService: CountryService;
  let categoryService: CategoryService;
  let translationService: TranslationService;
  let store: Store<AppState>;

  beforeEach(async(() => {
    countryService = jasmine.createSpyObj('CountryService', ['getList']);
    categoryService = jasmine.createSpyObj('CategoryService', ['getList']);
    translationService = jasmine.createSpyObj('TranslationService', ['getList']);
    store = jasmine.createSpyObj('Store', ['select', 'dispatch']);

    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      providers: [
        {provide: Store, useCuseValuelass: store},
        {provide: CountryService, useValue: countryService},
        {provide: CategoryService, useValue: categoryService},
        {provide: TranslationService, useValue: translationService},
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeDefined();
  }));
  it(`should have as title 'Emma Frontend'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const html: HTMLElement = fixture.nativeElement;
    expect(html.querySelector('a.navbar-brand').textContent).toEqual('EMMA');
  }));
});
