import {Component, OnInit} from '@angular/core';
import {CategoryService, CountryService, TranslationService} from '../services';
import {Country} from '../models/country.model';
import {Category} from '../models/category.model';
import {Phrase} from '../models/phrase.model';
import 'rxjs/add/operator/distinctUntilChanged';
import { Observable} from 'rxjs';
import { Translation } from '../models/translation.model';
import { Store } from '@ngrx/store';
import * as fromStore from '../store';

@Component({
    selector: 'app-root',
    template: `
        <nav class='navbar navbar-expand-sm navbar-expand-lg navbar-fixed-top navbar-dark bg-dark justify-content-between mb-3'>
            <button
                    class='navbar-toggler'
                    type='button'
                    data-toggle='collapse'
                    data-target='#navbarSupportedContent'
                    aria-controls='navbarSupportedContent'
                    aria-expanded='false'
                    aria-label='Toggle navigation'>
                <span class='navbar-toggler-icon'></span>
            </button>
            <a href='#' class='navbar-brand'>EMMA</a>
            <span *ngIf='currentCountry' class='navbar-text'>
                <img class='mr-2'
                     width='25px' height='25px'
                     src='assets/svg-country-flags/{{currentCountry.languageKey}}.svg'
                     alt='{{currentCountry.name}}'
                     title='Wähle {{currentCountry.name}}'/>
                {{currentCountry.name}}
            </span>
            <form class='form-inline'>
                <button type='button'
                        class='btn btn-outline-light my-2 my-sm-0'
                        (click)='toggleCountrySelectionVisibility()'>{{toggleCountryButtonText}}
                </button>
            </form>
        </nav>
        <app-country-selection
                [countries]='countries$ | async'
                [isVisible]='isCountrySelectVisible'
                (selectCountry)='onSelectCountry($event)'></app-country-selection>
        <app-phrase-collection
                [phrases]='phrases$ | async'
                [categories]='categories$ | async'
                [selectedCountry]='selectedCountry$ | async'
                (createPhrase)='pushPhrase($event)'></app-phrase-collection>
    `
})
export class AppComponent implements  OnInit {
    isCountrySelectVisible: boolean;
    toggleCountryButtonText: string;
    currentCountry: Country;
    countries$: Observable<Country[]>;
    categories$: Observable<Category[]>;
    phrases$: Observable<Phrase[]>;
    translations$: Observable<Translation[]>;

    selectedCountry$: Observable<Country>;

    public constructor(
        private store: Store<fromStore.AppState>,
        private countryService: CountryService,
        private translationService: TranslationService) {}

    ngOnInit(): void {
        this.countries$ = this.countryService.getList();

        this.categories$ = this.store.select(fromStore.getAllCategories);
        this.phrases$ = this.store.select(fromStore.getAllPhrases);
        this.selectedCountry$ = this.store.select(fromStore.getSelectedCountry);

        this.translations$ = this.translationService.getList();

        this.store.dispatch(new fromStore.LoadPhrasesAction());
        this.store.dispatch(new fromStore.LoadCategoriesAction());

        this.isCountrySelectVisible = true;

        this.toggleButtonText();
    }

    onSelectCountry(country: Country): void {
        this.currentCountry = country;
        this.isCountrySelectVisible = false;
        this.toggleButtonText();
    }

    pushPhrase(phrase: Phrase): void {
        this.store.dispatch(new fromStore.CreateNewPhraseAction(phrase));
    }

    toggleCountrySelectionVisibility(): void {
        this.isCountrySelectVisible = !this.isCountrySelectVisible;
        this.toggleButtonText();
    }

    toggleButtonText(): void {
        this.toggleCountryButtonText = 'Show Country Selection';

        if (this.isCountrySelectVisible) {
            this.toggleCountryButtonText = 'Hide Country Selection';
        }
    }
}
