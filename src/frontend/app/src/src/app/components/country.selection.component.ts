import {Component, Input, ChangeDetectionStrategy, Output, EventEmitter} from '@angular/core';
import { Country } from '../models/country.model';
import * as fromStore from '../store';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-country-selection',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class="container">
            <div class="row" *ngIf="isVisible">
                <div class="col card-deck">
                    <app-country-selection-item
                        [country]="country"
                        *ngFor="let country of countries"
                        (selectCountry)="onSelectCountry($event)"></app-country-selection-item>
                </div>
            </div>
        </div>
    `
})
export class CountrySelectionComponent {
    @Input() isVisible = true;
    @Input() countries: Country[] = [];
    @Output() selectCountry: EventEmitter<Country> = new EventEmitter<Country>();

    constructor (private store: Store<fromStore.AppState>) {}

    onSelectCountry(country: Country): void {
        this.isVisible = false;
        this.store.dispatch(new fromStore.SelectCountry(country));
        this.selectCountry.emit(country);
    }
}
