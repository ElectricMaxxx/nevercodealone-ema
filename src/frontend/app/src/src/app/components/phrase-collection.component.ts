import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ChangeDetectionStrategy} from '@angular/core';
import {Country} from '../models/country.model';
import {Phrase} from '../models/phrase.model';
import {Category} from '../models/category.model';

@Component({
    selector: 'app-phrase-collection',
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class='container' *ngIf="isCountrySelected()">
            <div class="row">
                <div class="col-4 phrase-category" *ngFor="let categoryWithPhrase of sortedByCategory">
                    <h4>{{categoryWithPhrase.category.category}}</h4>
                    <app-phrase-item
                        [phrase]="phrase"
                        [selectedCountry]="selectedCountry"
                        *ngFor="let phrase of categoryWithPhrase.phrases"></app-phrase-item>
                </div>
            </div>
            <div class="form-container col-md-4">
                <form>
                    <div class="form-group">
                        <label for="category-select">Category</label>
                        <ng-select required
                                name="category-select"
                                [items]="categories"
                                [multiple]="true"
                                bindLabel="category"
                                [closeOnSelect]="false"
                                bindValue="id"
                                [(ngModel)]="selectedPhrase.category">
                            <ng-template ng-option-tmp let-item="item" let-item$="item$" let-index="index">
                                <input id="item-{{index}}" type="checkbox" /> {{item.category}}
                            </ng-template>
                        </ng-select>
                    </div>
                    <div class="form-group">
                        <label for="name">Phrase</label>
                        <input type="text" class="form-control" required [(ngModel)]="selectedPhrase.name" name="phrase">
                    </div>
                    <button
                            type="submit"
                            class="btn btn-primary mr-1"
                            [disabled]="createNewIsDisabled()"
                            (onclick)="onCreate()">Übersetzen</button>
                    <button type="reset" class="btn btn-secondary" (click)="resetForm()">Reset</button>
                </form>
            </div>
        </div>
    `
})
export class PhraseCollectionComponent implements OnInit, OnChanges {
    @Input() selectedCountry: Country;
    @Input() phrases: Phrase[] = [];
    @Input() categories: Category[] = [];

    @Output() createPhrase = new EventEmitter<Phrase> ();

    sortedByCategory: {category: Category, phrases: Phrase[]}[] = [];

    selectedPhrase: Phrase;

    ngOnInit(): void {
        this.sortByCategory();
        this.resetForm();
    }

    private resetForm() {
        this.selectedPhrase = {
            id: null,
            name: '',
            category: null,
            translations: []
        };
    }

    createNewIsDisabled(): boolean {
        return this.selectedPhrase.category === null;
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.sortByCategory();
    }

    onCreate(): void {
        this.createPhrase.emit(this.selectedPhrase);
        this.resetForm();
    }



    public isCountrySelected(): boolean {
        return this.selectedCountry && this.selectedCountry.name !== null;
    }

    private sortByCategory(): void {
        if (null === this.phrases || null === this.categories) {
            return;
        }

        const entities = this.phrases.reduce((list, item): {[id: number]: {category: Category, phrases: Phrase[]}} => {
            if (!list[item.category.id]) {
                list[item.category.id] = {category: item.category, phrases: []};
            }
            list[item.category.id].phrases.push(item);
            return list;
        }, {});
        this.sortedByCategory = Object.keys(entities).map(id => entities[parseInt(id, 10)]);
    }
}
