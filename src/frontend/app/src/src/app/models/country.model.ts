export interface Country {
    id?: number;
    name: string;
    languageKey: string;
}
