import {Category} from './category.model';
import {Translation} from './translation.model';

export interface Phrase {
    id?: number;
    name: string;
    category: Category;
    translations: Translation[];
}
