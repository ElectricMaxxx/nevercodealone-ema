export interface Translation {
    id?: number;
    languageKey: string;
    translation: string;
}
