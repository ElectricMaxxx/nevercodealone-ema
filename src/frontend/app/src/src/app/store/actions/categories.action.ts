import { Action } from '@ngrx/store';
import { Category } from '../../models/category.model';

export const LOAD_CATEGORIES = '[categories] Load categories';
export const LOAD_CATEGORIES_SUCCESS = '[categories] Load categories succeeded';
export const LOAD_CATEGORIES_FAIL = '[categories] Load categories failed';

export const CREATE_NEW_CATEGORY = '[categories] create new category';
export const CREATE_NEW_CATEGORY_SUCCESS = '[categories] create new category succeeded';
export const CREATE_NEW_CATEGORY_FAIL = '[categories] create new category failed';

export class LoadCategoriesAction implements Action {
    readonly type = LOAD_CATEGORIES;
    constructor() { }
}

export class LoadCategoriesSuccessAction implements Action {
    readonly type = LOAD_CATEGORIES_SUCCESS;
    constructor(public payload: Category[]) { }
}

export class LoadCategoriesFailAction implements Action {
    readonly type = LOAD_CATEGORIES_FAIL;
    constructor(public payload: any) { }
}

export class CreateNewCategoryAction implements Action {
    readonly type = CREATE_NEW_CATEGORY;
    constructor(public payload: Category) { }
}

export class CreateNewCategorySuccessAction implements Action {
    readonly type = CREATE_NEW_CATEGORY_SUCCESS;
    constructor(public payload: Category) { }
}

export class CreateNewCategoryFailAction implements Action {
    readonly type = CREATE_NEW_CATEGORY_FAIL;
    constructor(public payload: any) { }
}
export type CategoriesActions =
    LoadCategoriesAction |
    LoadCategoriesSuccessAction |
    LoadCategoriesFailAction |
    CreateNewCategoryAction |
    CreateNewCategorySuccessAction |
    CreateNewCategoryFailAction;
