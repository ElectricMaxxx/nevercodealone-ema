import * as fromCategories from './categories.action';

describe('CategoriesActions', () => {
    describe('LoadCategories', () => {
        it('should create a LoadCategoriesAction', () => {
            const action = new fromCategories.LoadCategoriesAction();

            expect(action.type).toEqual(fromCategories.LOAD_CATEGORIES);
        });
    });
    describe('CreateNewCategory', () => {
        it('should create a CreateNewCategoryAction', () => {
            const payload = {category: 'test-category', description: 'test-desc'};
            const action = new fromCategories.CreateNewCategoryAction(payload);

            expect(action.type).toEqual(fromCategories.CREATE_NEW_CATEGORY);
            expect(action.payload).toEqual(payload);
        });
        it('should create a CreateNewCategorySuccessAction', () => {
            const payload = {id: 1, category: 'test-category', description: 'test-desc'};
            const action = new fromCategories.CreateNewCategorySuccessAction(payload);

            expect(action.type).toEqual(fromCategories.CREATE_NEW_CATEGORY_SUCCESS);
            expect(action.payload).toEqual(payload);
        });
        it('should create a CreateNewCategoryFailAction', () => {
            const fail = 'fail';
            const action = new fromCategories.CreateNewCategoryFailAction(fail);

            expect(action.type).toEqual(fromCategories.CREATE_NEW_CATEGORY_FAIL);
            expect(action.payload).toEqual(fail);
        });
    });
});
