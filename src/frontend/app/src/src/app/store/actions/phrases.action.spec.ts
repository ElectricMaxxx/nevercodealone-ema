import { SelectCountry, SELECT_COUNTRY } from './view.action';
import * as fromPhrases from './phrases.action';

describe('PhrasesActions', () => {
    describe('LoadPhrases', () => {
        it('should create a LoadPhrasesAction', () => {
            const action = new fromPhrases.LoadPhrasesAction();

            expect(action.type).toEqual(fromPhrases.LOAD_PHRASES);
        });
    });
    describe('CreateNewPhrase', () => {
        it('should create a CreateNewPhraseAction', () => {
            const payload = {name: 'test-name', category: null, translations: []};
            const action = new fromPhrases.CreateNewPhraseAction(payload);

            expect(action.type).toEqual(fromPhrases.CREATE_NEW_PHRASE);
            expect(action.payload).toEqual(payload);
        });
        it('should create a CreateNewPhraseSuccessAction', () => {
            const payload = {id: 1, name: 'test-name', category: null, translations: []};
            const action = new fromPhrases.CreateNewPhraseSuccessAction(payload);

            expect(action.type).toEqual(fromPhrases.CREATE_NEW_PHRASE_SUCCESS);
            expect(action.payload).toEqual(payload);
        });
        it('should create a CreateNewPhraseSuccessAction', () => {
            const fail = 'fail';
            const action = new fromPhrases.CreateNewPhraseFailAction(fail);

            expect(action.type).toEqual(fromPhrases.CREATE_NEW_PHRASE_FAIL);
            expect(action.payload).toEqual(fail);
        });
    });
});
