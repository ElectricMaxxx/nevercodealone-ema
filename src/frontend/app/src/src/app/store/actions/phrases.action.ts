import { Action } from '@ngrx/store';
import { Phrase } from '../../models/phrase.model';

export const LOAD_PHRASES = '[phrases] Load phrases';
export const LOAD_PHRASES_SUCCESS = '[phrases] Load phrases succeded';
export const LOAD_PHRASES_FAIL = '[phrases] Load phrases failed';


export const CREATE_NEW_PHRASE = '[phrases] create new phrase';
export const CREATE_NEW_PHRASE_SUCCESS = '[phrases] create new phrase succeded';
export const CREATE_NEW_PHRASE_FAIL = '[phrases] create new phrase failed';


export class LoadPhrasesAction implements Action {
    readonly type = LOAD_PHRASES;
    constructor() { }
}

export class LoadPhrasesSuccessAction implements Action {
    readonly type = LOAD_PHRASES_SUCCESS;
    constructor(public payload: Phrase[]) { }
}

export class LoadPhrasesFailAction implements Action {
    readonly type = LOAD_PHRASES_FAIL;
    constructor(public payload: any) { }
}

export class CreateNewPhraseAction implements Action {
    readonly type = CREATE_NEW_PHRASE;
    constructor(public payload: Phrase) { }
}

export class CreateNewPhraseSuccessAction implements Action {
    readonly type = CREATE_NEW_PHRASE_SUCCESS;
    constructor(public payload: Phrase) { }
}

export class CreateNewPhraseFailAction implements Action {
    readonly type = CREATE_NEW_PHRASE_FAIL;
    constructor(public payload: any) { }
}

export type PhrasesActions =
    LoadPhrasesAction |
    LoadPhrasesSuccessAction |
    LoadPhrasesFailAction |
    CreateNewPhraseAction |
    CreateNewPhraseSuccessAction |
    CreateNewPhraseFailAction;
