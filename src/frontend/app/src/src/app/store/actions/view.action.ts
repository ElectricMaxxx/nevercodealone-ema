import { Action } from '@ngrx/store';
import { Country } from '../../models/country.model';

export const SELECT_COUNTRY = '[view] select a contry.';

export class SelectCountry implements Action {
    readonly type = SELECT_COUNTRY;
    constructor (public payload: Country) {}
}

export type viewActions = SelectCountry;
