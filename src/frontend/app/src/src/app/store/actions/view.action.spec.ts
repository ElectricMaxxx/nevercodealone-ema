import { SelectCountry, SELECT_COUNTRY } from './view.action';

describe('SelectViewAction', () => {
    describe('SelectCountry', () => {
        it('should create a valid SelectCountry object', () => {
            const action = new SelectCountry({name: 'test', languageKey: 'de_DE'});

            expect(action.type).toEqual(SELECT_COUNTRY);
            expect(action.payload).toEqual({name: 'test', languageKey: 'de_DE'});
        });
    });
});
