import {createSelector} from '@ngrx/store';

import * as fromView from '../reducers/view.reducer';
import * as fromFeature from '../reducers';

export const getViewState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.view
);

export const getSelected = createSelector(
    getViewState,
    fromView.getSelected
);

export const getSelectedCountry = createSelector(
    getSelected,
    (selected: fromView.Selected) => selected.country
);
