import * as fromPhrases from '../actions/phrases.action';
import { Phrase } from '../../models/phrase.model';

export interface PhrasesState {
    entities: {[id: number]: Phrase};
    loaded: boolean;
    loading: boolean;
}

export const initialState: PhrasesState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer (
    state = initialState,
    action: fromPhrases.PhrasesActions
): PhrasesState {
    switch (action.type) {
        case fromPhrases.CREATE_NEW_PHRASE:
        case fromPhrases.LOAD_PHRASES:
            return {
                ...state,
                loading: true,
            };
        case fromPhrases.LOAD_PHRASES_SUCCESS:
            const phrases = action.payload;

            return {
                ...state,
                entities: phrases.reduce((entities: { [id: number]: Phrase }, phrase: Phrase) => {
                    return { ...entities, [phrase.id]: phrase};
                }, { ...state.entities}),
                loaded: true,
                loading: false
            };
        case fromPhrases.CREATE_NEW_PHRASE_SUCCESS:
            const phrase = action.payload;

            return {
                ...state,
                entities: {...state.entities, [phrase.id]: phrase},
                loaded: false,
                loading: false
            };
        case fromPhrases.CREATE_NEW_PHRASE_FAIL:
        case fromPhrases.LOAD_PHRASES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false
            };
    }

    return state;
}

export const getAllPhraseEntities = (state: PhrasesState) => state.entities;
export const getLoading = (state: PhrasesState) => state.loading;
export const getLoaded = (state: PhrasesState) => state.loaded;
