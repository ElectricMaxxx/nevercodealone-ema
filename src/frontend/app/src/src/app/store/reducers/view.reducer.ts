import * as fromView from '../actions/view.action';
import { Country } from '../../models/country.model';

export interface Selected {
    country: Country;
}

export interface ViewState {
    selected: Selected;
}

const initialSelected: Selected = {
    country: {name: null, languageKey: null}
};

export const initialState: ViewState = {
    selected: initialSelected
};

export function reducer(
    state = initialState,
    action: fromView.viewActions): ViewState {
    switch (action.type) {
        case fromView.SELECT_COUNTRY:
            const selected: Selected = { ...state.selected, country: action.payload};

            return {
                ...state,
                selected
            };
    }

    return state;
}

export const getSelected = (state: ViewState) => state.selected;
