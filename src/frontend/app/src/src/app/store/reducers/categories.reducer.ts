import * as fromCategories from '../actions/categories.action';
import { Category } from '../../models/category.model';

export interface CategoriesState {
    entities: {[id: number]: Category};
    loaded: boolean;
    loading: boolean;
}

export const initialState: CategoriesState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer (
    state = initialState,
    action: fromCategories.CategoriesActions
): CategoriesState {
    switch (action.type) {
        case fromCategories.CREATE_NEW_CATEGORY:
        case fromCategories.LOAD_CATEGORIES:
            return {
                ...state,
                loading: true,
            };
        case fromCategories.LOAD_CATEGORIES_SUCCESS:
            const categories = action.payload;

            return {
                ...state,
                entities: categories.reduce((entities: { [id: number]: Category }, category: Category) => {
                    return { ...entities, [category.id]: category};
                }, { ...state.entities}),
                loaded: true,
                loading: false
            };
        case fromCategories.CREATE_NEW_CATEGORY_SUCCESS:
            const category = action.payload;

            return {
                ...state,
                entities: {...state.entities, [category.id]: category},
                loaded: false,
                loading: false
            };
        case fromCategories.CREATE_NEW_CATEGORY_FAIL:
        case fromCategories.LOAD_CATEGORIES_FAIL:
            return {
                ...state,
                loading: false,
                loaded: false
            };
    }

    return state;
}


export const getAllCategoryEntities = (state: CategoriesState) => state.entities;
export const getLoading = (state: CategoriesState) => state.loading;
export const getLoaded = (state: CategoriesState) => state.loaded;
