import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromCategories from './categories.reducer';
import * as fromPhrases from './phrases.reducer';
import * as fromSelected from './view.reducer';

export interface AppState {
    phrases: fromPhrases.PhrasesState;
    categories: fromCategories.CategoriesState;
    view: fromSelected.ViewState;
}

export const reducers: ActionReducerMap<AppState> = {
    categories: fromCategories.reducer,
    phrases: fromPhrases.reducer,
    view: fromSelected.reducer
};


export const getAppState = createFeatureSelector<AppState>('app');
