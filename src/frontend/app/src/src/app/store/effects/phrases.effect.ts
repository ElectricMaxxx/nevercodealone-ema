import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as phrasesAction from '../actions/phrases.action';
import * as fromServices from '../../services';
import {Phrase} from '../../models/phrase.model';

@Injectable()
export class PhrasesEffect {
    constructor(
        private actions$: Actions,
        private phrasesService: fromServices.PhraseService
    ) {}

    @Effect()
    loadPhrases$ = this.actions$.pipe(
        ofType(phrasesAction.LOAD_PHRASES),
        switchMap(() => {
            return this.phrasesService.getList()
                .pipe(
                    map((phrases: Phrase[]) => new phrasesAction.LoadPhrasesSuccessAction(phrases)),
                    catchError(error => of(new phrasesAction.LoadPhrasesFailAction(error)))
                );
        })
    );

    @Effect()
    createNewPhrase$ = this.actions$.pipe(
        ofType(phrasesAction.CREATE_NEW_PHRASE),
        map((action: phrasesAction.CreateNewPhraseAction) => action.payload),
        switchMap((phrase: Phrase) => {
            return this.phrasesService.addPhrase(phrase)
                .pipe(
                    map((newPhrase: Phrase) => new phrasesAction.CreateNewPhraseSuccessAction(newPhrase)),
                    catchError(error => of(new phrasesAction.CreateNewPhraseFailAction(error)))
                );
        })
    );
}
