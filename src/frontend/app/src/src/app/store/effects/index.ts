import { CategoriesEffect } from './categories.effect';
import { PhrasesEffect } from './phrases.effect';

export const effects = [CategoriesEffect, PhrasesEffect];

export * from './categories.effect'
export * from './phrases.effect';
