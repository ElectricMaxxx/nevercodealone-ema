import { Injectable } from '@angular/core';

import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import * as categoriesAction from '../actions/categories.action';
import * as fromServices from '../../services';
import {Category} from '../../models/category.model';

@Injectable()
export class CategoriesEffect {
    constructor(
        private actions$: Actions,
        private categoryService: fromServices.CategoryService
    ) {}

    @Effect()
    loadCategories$ = this.actions$.pipe(
        ofType(categoriesAction.LOAD_CATEGORIES),
        switchMap(() => {
            return this.categoryService.getList()
                .pipe(
                    map((categories: Category[]) => new categoriesAction.LoadCategoriesSuccessAction(categories)),
                    catchError(error => of(new categoriesAction.LoadCategoriesFailAction(error)))
                );
        })
    );

    @Effect()
    createNewCategory$ = this.actions$.pipe(
        ofType(categoriesAction.CREATE_NEW_CATEGORY),
        map((action: categoriesAction.CreateNewCategoryAction) => action.payload),
        switchMap((category: Category) => {
            return this.categoryService.addCategory(category)
                .pipe(
                    map((newCategory: Category) => new categoriesAction.CreateNewCategorySuccessAction(newCategory)),
                    catchError(error => of(new categoriesAction.CreateNewCategoryFailAction(error)))
                );
        })
    );
}
