# Emma Angular Application


This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Before starting the application make sure you run `cd src/frontend/ && npm install`. To get a lightweight backend run `npm run json-server` and the your are ready to start the app.

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
