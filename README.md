# Emma Version 2

This is a complete rewrite of the former [Emma](http://emma.nevercodealone.de/). You will finde a standalone [angular frontend](src/frontend) and its [backend api](src/backend) combined through a docker setup.

## Usage

### Angular frontend

To run and build the fronend app follow instruction [here](src/frontend/README.md)

### API Application

To run and build the api application follow instructions [here](src/backend/README.md)

### All in One - Docker Setup

To run the current docker setup to have komplete applicatin running, use `docker-compose.yml` and run `docker-compose up -d`
*hint*: We will switch docker setup later, to have dev and production version.