include Makefile.docker

.PHONY: unit_test, api_test, release
unit_test:
	@echo "+++ Unit tests +++"
override projectRootDir = .
override projectVersionFile = $(projectRootDir)/VERSION
override projectVersion = $(shell head -n1 $(projectVersionFile))
override gitOriginUrl = $(shell git config --get remote.origin.url)
override projectRegistry=$(REGISTRY)
override projectPath=$(REPOSITORY_PATH)
override baseImage = $(REGISTRY)/$(REPOSITORY_PATH)/app-$(RUNTIME):$(projectVersion)
override containerBasePath=$(REGISTRY)/$(REPOSITORY_PATH)/app-$(RUNTIME)

# helper macros
override getImage = $(firstword $(subst :, ,$1))
override getImageTag = $(or $(word 2,$(subst :, ,$1)),$(value 2))
override printRow = @printf "%+22s = %-s\n" $1 $2

override M4_OPTS = \
	--define m4ProjectName=$(projectName) \
	--define m4ProjectVersion=$(projectVersion) \
	--define m4GitOriginUrl=$(gitOriginUrl) \
	--define m4BaseImage=$(call getImage, $(baseImage)) \
	--define m4BaseImageTag=$(call getImageTag, $(baseImage),latest) \
	--define m4ContainerBasePath=$(containerBasePath)

kubernetes_image_versions:
	@m4 $(M4_OPTS) $(projectRootDir)/kubernetes/frontend.production.m4.yaml > $(projectRootDir)/kubernetes/frontend.production.yaml
	@m4 $(M4_OPTS) $(projectRootDir)/kubernetes/phrases.production.m4.yaml > $(projectRootDir)/kubernetes/phrases.production.yaml


release:
	@echo "Release next version: $(VERSION_TAG)"
	@echo $(VERSION_TAG) > ./VERSION
	@make kubernetes_image_versions
	@git add .
	@git commit -m "Changes for next release $(VERSION_TAG)"
	@git tag -s $(VERSION_TAG) -m "Next release $(VERSION_TAG)"
	@git push --tags origin master


###############################################################################
## Setup the stage                                                           ##
###############################################################################
composer:
	docker exec $(COMPOSE_PROJECT_NAME)-api sh -c "composer install -d /app/src;"

clear_cache:
	  # force clearing the cache
	  docker exec $(COMPOSE_PROJECT_NAME)-api sh -c "rm -rf var/cache/dev && rm -rf var/cache/test"
	  docker exec $(COMPOSE_PROJECT_NAME)-api sh -c "cd /app/src/ && php -d memory_limit=-1 bin/console cache:warmup -n  -vvv"

###############################################################################
## Testing                                                                   ##
###############################################################################

sniff:
	docker exec $(COMPOSE_PROJECT_NAME)-api sh -c "echo 'Do sniff'"

unit_test:
	@echo "+++ Unit tests +++"

api_test:
	echo "+++ API tests i.e with behat+++

.PHONY: test
test: sniff
	@echo
	@echo "+++ Complete +++"
	make unit_test
